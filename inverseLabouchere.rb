#!/usr/bin/ruby
#coding: utf-8

sistema = [1,2,3,4] # Labrouchere Inverso
numeros = []

ronda = 0
mayorCifra = 0
topReinicio = 100

print "\n¿Con cuánto dinero deseas empezar? -> "
dinero = gets.chomp; dinero = dinero.to_i
dinero_backup = dinero
topReinicio += dinero
print "\n¿Total de jugadas? -> "
jugadas = gets.chomp

jugadas.to_i.times { |x| numeros.push(rand(36)) } # Vector de números creado

print "\nNúmeros Registrados:\n\n#{numeros}\n"

labrouchereInverso = Proc.new do |number|
	ronda += 1
	puts "\nRonda número [#{ronda}] || Dinero en el bolsillo: #{dinero}.00 euros\n"

	mayorCifra = dinero if mayorCifra < dinero
	apuesta = sistema[0].to_i + sistema[-1].to_i
	apuesta = sistema[0] if sistema.size == 1
	dinero -= apuesta.to_i
	
	puts "Sale el número #{number}, hemos aplicado técnica #{sistema}, apostamos #{apuesta}.00 euros y nos quedamos con #{dinero}.00 euros"

	if number % 2 == 0 && number != 0
		dinero += apuesta*2
		sistema.push(apuesta)
	elsif number % 2 != 0 || number == 0
		sistema.pop
		sistema.shift
	end

	sistema = [1,2,3,4] if sistema.size == 0

	if dinero >= topReinicio
		topReinicio = dinero + 100
		sistema = [1,2,3,4]
	end

	if dinero.to_i <= 0 || ronda.to_i == jugadas.to_i
		puts "\n=> Tope al que se ha llegado con la técnica: #{mayorCifra}.00 euros\n"
	end

	if ronda.to_i == jugadas.to_i
		print "\n=> Si te hubieras ido en #{jugadas} jugadas, te habrías ido con #{dinero}.00 euros, "

		puts dinero.to_i > dinero_backup.to_i ? "#{dinero.to_i - dinero_backup.to_i}.00 euros más respecto a los que tenías en un principio\n\n" : "#{dinero_backup.to_i - dinero.to_i} euros menos respecto a los que tenías en un principio\n\n" 
	end
end

numeros.collect(&labrouchereInverso) if dinero.to_i > 0

