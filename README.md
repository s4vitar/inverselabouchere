# inverseLabouchere

![InverseLabouchere](Images/ruleta.png)

**Labouchere Inverso**, también conocido como método de cancelación inverso, se trata de una técnica que consiste en apostar una serie de números los cuales iremos cancelando en caso de perder y añadiendo en caso de ganar.

Ejemplo (Apuestas a Par - Suerte Sencilla || Los beneficios equivalen al doble de lo apostado) [<b>Saldo base: 50 €</b>]:

| Sistema de Apuesta  | Apuesta | Número | Dinero Actual
| ------------- | ------------- | ------------- | ------------- |
| **[1,2,3,4]**  | 5€ (Sumamos extremos) | 21 | 45 € |
| **[2,3]**  | 5€ (Cancelamos extremos y sumamos extremos)  | 26 | 50€ |
| **[2,3,5]**  | 7€ (Añadimos beneficio y sumamos extremos)  | 22 | 57€ |
| **[2,3,5,7]**  | 9€ (Añadimos beneficio y sumamos extremos)  | 13 | 48€ |
| **[3,5]**  | 8€ (Cancelamos extremos y sumamos extremos)  | 26 | 56€ |
| **[3,5,8]**  | 11€ (Añadimos beneficio y sumamos extremos)  | 2 | 67€ |
| **[3,5,8,11]**  | 14€ (Añadimos beneficio y sumamos extremos)  | 32 | 81€ |
| **[3,5,8,11,14]**  | 17€ (Añadimos beneficio y sumamos extremos)  | 25 | 64€ |
| **[5,8,11]**  | 16€ (Cancelamos extremos y sumamos extremos)  | 1 | 48€ |
| **[8]**  | 8€ (Cancelamos extremos y apostamos unidad)  | 22 | 56€ |
| **[8,8]**  | 16€ (Añadimos beneficio y sumamos extremos)  | 4 | 72€ |
| **[8,8,16]**  | 24€ (Añadimos beneficio y sumamos extremos)  | 2 | 96€ |
| **[8,8,16,24]**  | 32€ (Añadimos beneficio y sumamos extremos)  | 10 | 128€ |
| **[8,8,16,24,32]**  | 40€ (Añadimos beneficio y sumamos extremos)  | 5 | 88€ |
| **[8,16,24]**  | 32€ (Cancelamos extremos y sumamos extremos)  | 6 | 120€ |
| **[8,16,24,32]**  | 40€ (Añadimos beneficio y sumamos extremos)  | 2 | 160€ |
| **[8,16,24,32,40]**  | 48€ (Añadimos beneficio y sumamos extremos)  | 3 | 112€ |
| **[16,24,32]**  | 48€ (Cancelamos extremos y sumamos extremos)  | 19 | 64€ |
| **[24]**  | 24€ (Cancelamos extremos y apostamos unidad)  | 32 | 88€ |
| **[24,24]**  | 48€ (Añadimos beneficio y sumamos extremos)  | 16 | 136€ |
| **[24,24,48]**  | 72€ (Añadimos beneficio y continuamos...)  | 26 | 208€ |

Lo bueno de esta técnica es que en caso de que nuestro vector del sistema de apuestas se quede nulo, podremos volver a empezar con un nuevo vector de apuestas <b>[1,2,3,4]</b>, habiendo únicamente perdido 10€ para la jugada con dicho vector.

La ventaja de esta técnica frente a otras, es que controlamos las proporciones de pérdidas, no obsesionándonos por tratar de recuperar lo que se ha perdido. La desventaja, al igual que el resto de sistemas, reside en que ningún sistema es perfecto.
